package br.unb.biodyn.modeltest;

import br.unb.biodyn.model.FormatTime;
import junit.framework.TestCase;

public class FormatTimeTest extends TestCase{

	private FormatTime formatTime;
	
	@Override
	protected void setUp()throws Exception {
		super.setUp();
		
		this.formatTime = new FormatTime();
	}
	
	public void testConvertionOfMinutesAndSeconds() throws Exception {
		
		long duration = 90000;
		
		assertEquals("1:30", this.formatTime.formatTimeMinutesAndSeconds(duration));		
	}
		
	public void testFormatTimeToSeconds() throws Exception {
		
		long duration = 90000;
		
		assertEquals("090", this.formatTime.formatTimeSeconds(duration));
	}
}
