package br.unb.biodyn.model;

import br.unb.biodyn.Communicator;

public abstract class Exercise extends ExerciseData implements ExerciseInterface {

	private boolean calledBioFeedback = false;
	
	public Exercise(ExerciseData data) {
		super(data.getMaximumWeight(), data.getMinimumWeight(), data
				.getRepetitions(), data.getSeries());
		reset();
	}

	public void computeData(float data) {
		if (checkRepetition(data))
			countDownRepetitions();
		checkMinimumBioFeedback(data);
		checkMaximumBioFeedback(data);
	}
	
	public void checkMinimumBioFeedback (float realWeight) {
		
		LogFormat.setLogMessage(LogFormat.EXERCISE, "Value of realWeight in checkMinimumRange(): " 
				+ realWeight);
		
		LogFormat.setLogMessage(LogFormat.EXERCISE, "Value of minimumWeight in checkMinimumRange(): "
				+ minimumWeight);
		
	

		if (realWeight < minimumWeight) {
			
			LogFormat.setLogMessage(LogFormat.EXERCISE, "Sending feedback that is below minimumWeight.");

			Communicator
					.getInstance()
					.sendToBioFeedback(
							Communicator.BioFeedbackServiceActions.ACTION_START,
							Communicator.BioFeedbackServiceActions.FLAG_VIBRATION,
							1000);
			calledBioFeedback = true;
		} else {
			calledBioFeedback = false;
		}
	}
	
	private void checkMaximumBioFeedback (float realWeight) {

		LogFormat.setLogMessage(LogFormat.EXERCISE, "maximumWeight: " + maximumWeight);

		if (realWeight > maximumWeight) {

			LogFormat.setLogMessage(LogFormat.EXERCISE, "Sending feedback that is above maximumWeight.");

			Communicator
					.getInstance()
					.sendToBioFeedback(
							Communicator.BioFeedbackServiceActions.ACTION_START,
							Communicator.BioFeedbackServiceActions.FLAG_VIBRATION,
							1000);
			calledBioFeedback = true;
		} else {
			calledBioFeedback = false;
		}

	} 
}
