package br.unb.biodyn.model;

public class IsometricExercise extends Exercise{

	private static final float DEFAULT_MAXIMUM_WEIGHT = 6;
	private static final float DEFAULT_MINIMUM_WEIGHT = 4;
	private static int DEFAULT_REPETITIONS = 10;
	private static int DEFAULT_SERIES = 5;

	public IsometricExercise(ExerciseData data) {
		super(data);
	}

	public IsometricExercise() {
		this(new ExerciseData(DEFAULT_MAXIMUM_WEIGHT,DEFAULT_MINIMUM_WEIGHT, DEFAULT_REPETITIONS, DEFAULT_SERIES));
	}

	@Override
	public boolean checkRepetition(float data) {
		// TODO implement this method
		return true;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
	}

	@Override
	public void countDownRepetitions() {
		// TODO Auto-generated method stub
	}

	@Override
	public void countDownSeries() {
		// TODO Auto-generated method stub
	}
	@Override
	public void stop() {
		// TODO Auto-generated method stub
	}
}
