package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.R;
import br.unb.biodyn.model.ExerciseData;
import br.unb.biodyn.ui.CounterUI.CounterUIListener;

public class LimitsWeightUI extends RelativeLayout implements CounterUIListener {
	
	private CounterUI counterSuperiorLimit;
	private CounterUI counterInferiorLimit;
	private TextView textRealWeight;

	
	public LimitsWeightUI(Context context, AttributeSet attrs) {
		super(context, attrs);
		inflate(context, R.layout.weight_limit_components, this);
		initializeComponents();
	}

	private void initializeComponents() {
		this.counterInferiorLimit = (CounterUI)findViewById(R.id.counter_inferior_limit);
		this.counterInferiorLimit.setCounterUIListener(this);
		this.counterSuperiorLimit = (CounterUI)findViewById(R.id.counter_superior_limit);
		this.counterSuperiorLimit.setCounterUIListener(this);
		this.textRealWeight = (TextView)findViewById(R.id.text_real_weight);
	}
	
	public Integer getSuperiorLimit() {
		return this.counterSuperiorLimit.getValue();
	}
	
	public Integer getInferiorLimit() {
		return this.counterInferiorLimit.getValue();
	}
	
	public CounterUI getTopCounter(){
		return counterSuperiorLimit;
	}
	
	public CounterUI getBottomCounter(){
		return counterInferiorLimit;
	}
	
	public Float getRealWeight() {
		return Float.valueOf(this.textRealWeight.getText().toString());
	}

	@Override
	public void onValueChange(CounterUI counterUI) {
		if(counterUI.getId() == this.counterSuperiorLimit.getId() ||
				counterUI.getId() == this.counterInferiorLimit.getId()) {
			sendUpdateLimits();
		}
	}

	private void sendUpdateLimits() {
		ExerciseData exerciseData = new ExerciseData();
		exerciseData.setMaximumWeight(this.counterSuperiorLimit.getValue());
		exerciseData.setMinimumWeight(this.counterInferiorLimit.getValue());
		Communicator.getInstance().sendToExerciseService(Communicator.FLAG_LIMITS, exerciseData);
	}
	
}
