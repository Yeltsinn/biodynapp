package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.model.ExerciseData;

public class CounterSeriesUI  extends CounterUI {

	public CounterSeriesUI(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public void increase() {
		super.increase();
		sendUpdateSeries();
	}
	
	@Override
	public void decrease() {
		super.decrease();
		sendUpdateSeries();
	}
	
	private void sendUpdateSeries() {
		ExerciseData exerciseData = new ExerciseData();
		exerciseData.setSeries(getValue());
		Communicator.getInstance().sendToExerciseService(Communicator.FLAG_SERIES, exerciseData);
	}

}
