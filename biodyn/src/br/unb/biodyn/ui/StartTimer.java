package br.unb.biodyn.ui;

import android.os.CountDownTimer;
import android.util.Log;

public class StartTimer extends CountDownTimer {

	public interface StartTimerListener {
		public void onTick(StartTimer startTimer, long millisUntilFinished);

		public void onFinish(StartTimer startTimer);
	}

	private static final long ONTICK_DELAY = 2000;
	protected StartTimerListener startTimerListener;

	public StartTimer(long duration, long onInterval) {
		super(ONTICK_DELAY + duration, onInterval);
	}

	@Override
	public void onTick(long millisUntilFinished) {
		notifyListenerOnTick(millisUntilFinished);
	}

	@Override
	public void onFinish() {
		notifyListenerOnFinish();
	}

	private void notifyListenerOnTick(long millisUntilFinished) {
		if (this.startTimerListener != null) {
			Log.d("debug", "notifyListenerOnTick");
			this.startTimerListener.onTick(this, millisUntilFinished);
		}
	}

	private void notifyListenerOnFinish() {
		if (this.startTimerListener != null) {
			this.startTimerListener.onFinish(this);
		}
	}

	public void setStartTimerListener(StartTimerListener startTimerListener) {
		this.startTimerListener = startTimerListener;
	}
}