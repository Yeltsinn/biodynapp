package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.R;
import br.unb.biodyn.model.FormatTime;
import br.unb.biodyn.ui.StartTimer.StartTimerListener;

public class TimerUI extends RelativeLayout implements OnClickListener,
		StartTimerListener {

	private static final long DEFAULT_TIME = 10000;
	private static final long ONE_SECOND = 1000;

	private TextView textTimerCounter;
	private Button buttonIncrease;
	private Button buttonDecrease;
	private StartTimer startTimer;

	private long duration;
	private long actualTime;

	private long onInterval = 1000;
	FormatTime formatterTime = new FormatTime();

	public TimerUI(Context context, AttributeSet attrs) {
		super(context, attrs);
		inflate(context, R.layout.counter_components, this);
		initializeComponents();
		this.startTimer = new StartTimer(actualTime, onInterval);
		this.startTimer.setStartTimerListener(this);
		setDuration(DEFAULT_TIME);
		updateTimeText();
	}

	private void initializeComponents() {
		this.textTimerCounter = (TextView) findViewById(R.id.text_counter);
		this.buttonIncrease = (Button) findViewById(R.id.button_increase);
		this.buttonIncrease.setOnClickListener(this);
		this.buttonDecrease = (Button) findViewById(R.id.button_decrease);
		this.buttonDecrease.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == this.buttonIncrease.getId()) {
			this.setDuration(this.actualTime + 1000);
			updateTimeText();
		}
		if (view.getId() == this.buttonDecrease.getId()) {
			this.setDuration(this.actualTime - 1000);
			updateTimeText();
		}
	}

	public void setDuration(long duration) {
		this.duration = duration;
		this.actualTime = duration;
		this.startTimer.cancel();
	}

	public void startTimer() {
		this.actualTime = this.duration;
		this.startTimer.cancel();
		this.startTimer = new StartTimer(this.duration, this.onInterval);
		this.startTimer.setStartTimerListener(this);
		this.startTimer.start();
	}

	public void stopTimer() {
		this.startTimer.cancel();
		actualTime = 0;
		updateTimeText();
	}

	private void updateTimeText() {
		this.textTimerCounter.setText(String.valueOf((formatterTime
				.formatTimeSeconds(this.actualTime))));
	}

	@Override
	public void onTick(StartTimer startTimer, long millisUntilFinished) {
		updateTimeText();
		actualTime = (actualTime - ONE_SECOND);
	}

	@Override
	public void onFinish(StartTimer startTimer) {
		textTimerCounter.setText("000");
		Communicator.getInstance().sendToBioFeedback(
				Communicator.BioFeedbackServiceActions.ACTION_START,
				Communicator.BioFeedbackServiceActions.FLAG_VIBRATION, 1000);
	}
}